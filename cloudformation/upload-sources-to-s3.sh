#!/usr/bin/env bash

scriptdir="$(cd "$(dirname "$0")"; pwd)"

bucket=helloworld-django

if ! aws s3 ls s3://"${bucket}" >/dev/null 2>&1; then
    aws s3 mb s3://"${bucket}"
fi

rm "${scriptdir}/django-app.zip"
pushd "${scriptdir}/.."
zip -r "${scriptdir}/django-app.zip" ./*
popd

aws s3 cp "${scriptdir}/django-app.zip" s3://"${bucket}/"
