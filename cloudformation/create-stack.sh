#!/usr/bin/env bash

scriptdir="$(cd "$(dirname "$0")"; pwd)"

[ -z "${1}" ] && cf_action='create-stack'
[[ "${1}" == 'update' ]] && cf_action='update-stack'

stack_name="django-app"
aws_profile="default"

# eip for couchdb
eip="52.20.133.236"

# pass db address to beanstalk
ebext="${scriptdir}/../.ebextensions"
rm -fr "${ebext}"; mkdir "${ebext}"
commands:
    00001_add_:
        cwd: /tmp
        command: 'sed -i "s/docker run -d/docker run -e "db_address=${eip}" -d/" /opt/elasticbeanstalk/hooks/appdeploy/enact/00run.sh'
EOF

# upload django app sources for beanstalk
"${scriptdir}/upload-sources-to-s3.sh"

aws cloudformation ${cf_action} \
    --stack-name "${stack_name}" \
    --template-body "file://${scriptdir}/cf.json" \
    --capabilities CAPABILITY_IAM \
    --parameters \
    ParameterKey="KeyName",ParameterValue="django-test-app" \
    ParameterKey="EIPAddress",ParameterValue="${eip}" \
    --profile "${aws_profile}"
