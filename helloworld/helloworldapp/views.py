from django.shortcuts import render
from django.http import HttpResponse
import os
import pycouchdb

def hello(request):
    db_info = get_db_info()
    return HttpResponse("Hello World!\n" + db_info)

def get_db_info():
    db_address = os.environ['db_address']
    db_port = '5984'
    db_uri = 'http://' + db_address + ':' + db_port
    try:
        db = pycouchdb.Server(db_uri)
        db_info = str(db.info())
    except:
        db_info = ''
    return db_info
