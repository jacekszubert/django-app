# Django helloworld application deployment

## Prerequisites
- awsebcli -- for Beanstalk part
- awscli -- for CloudFormation part
Can be installed with:
```
pip install awsebcli
pip install awscli
```
To interact with AWS environment you also need to provide basic configuration and proper IAM permissions to used user specified with:
```
aws configure
```

## Deployment with Beanstalk
- [eb init](docs/eb-init.log) -- initialize your directory with the EB CLI, required basic AWS information
- [eb create](docs/eb-create.log) -- create a new environment, execution takes few minutes
- [eb config](docs/eb-config.log) -- edit the environment configuration settings

## CouchDB integration
Parameters (such as DB address and credentials) can be passed to docker container using .ebextensions configuration files. Presented in extended with CloudFormation example.

## Deployment with CloudFormation
CloudFormation is an alternative service for Beanstalk, but can also easily be integrated with it. Scripts provided in [cloudformation](cloudformation) directory allow to create CloudFormation stack with resources:
- EC2 instance with CouchDB
- Beanstalk with dockerized example django app with connection to CouchDB

Since CF was not a scope of the task, provided stack is very simple, with no security (IAM, VPC, Security Groups configuration) or high availability for DB (which could be achieved by both CouchDB features and multiAZ deployment).
To create CF stack you need to create ElasticIP (for DB) manually and assign it to "eip" variable in [cloudformation/create-stack.sh](cloudformation/create-stack.sh). After this step run the script and whole stack will be deployed in around 10 minutes: creation of S3 bucket, upload of zip file with app sources and creation of CF stack. Connection to CouchDB is proven by printing basic information.

![Beanstalk](docs/beanstalk.png?raw=true)

![CloudFormation](docs/cloudformation.png?raw=true)

![Django app](docs/django.png?raw=true)
