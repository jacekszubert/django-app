FROM python:3.5.1

RUN apt-get update
RUN pip install --upgrade pip

ADD . sources

RUN pip install -r sources/helloworld/requirements.txt

EXPOSE 80

CMD ["python", "sources/helloworld/manage.py", "runserver", "0.0.0.0:80"]
